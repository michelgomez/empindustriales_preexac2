package com.example.empresasindustrialesmx;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

import Modelo.UsuariosDb;


public class MainActivity extends AppCompatActivity {
    private EditText txtCorreo, txtContra;
    private Button btnIngresar, btnRegistrarse, btnSalir;
    private UsuariosDb usuariosDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        usuariosDb = new UsuariosDb(this);
        txtCorreo = findViewById(R.id.txtCorreo);
        txtContra = findViewById(R.id.txtContraseña);
        btnIngresar = findViewById(R.id.btn_login);
        btnRegistrarse = findViewById(R.id.btn_register);
        btnSalir = findViewById(R.id.btn_exit);

        btnIngresar.setOnClickListener(v -> {
            String correo = txtCorreo.getText().toString();
            String contra = txtContra.getText().toString();

            Usuarios usuario = usuariosDb.getUsuario(correo);

            if (usuario != null && usuario.getPassword().equals(contra)){
                Toast.makeText(MainActivity.this, "Bienvenido", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, listaActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(MainActivity.this, "El usuario no es correcto o no se encuentra registrado", Toast.LENGTH_SHORT).show();

            }


        });

        btnRegistrarse.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, registroActivity.class);
            startActivity(intent);
        });

        btnSalir.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Usuarios");
            builder.setMessage("¿Seguro que desea salir?");
            builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            builder.setNegativeButton("No", null);
            AlertDialog dialog = builder.create();
            dialog.show();
        });
    }
}