package com.example.empresasindustrialesmx;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class UsuariosAdapter extends BaseAdapter {

    Context context;
    List<Usuarios> lst;

    public UsuariosAdapter(Context context, List<Usuarios> lst) {
        this.context = context;
        this.lst = lst;
    }

    @Override
    public int getCount() {
        return lst.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView TextViewUsuario, TextViewCorreo;

        Usuarios p = lst.get(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_usuario, null);
        }

        TextViewCorreo = convertView.findViewById(R.id.txtCorreo);
        TextViewUsuario = convertView.findViewById(R.id.txtNombreUsuario );

        TextViewCorreo.setText(p.correo);
        TextViewUsuario.setText(p.usuario);

        return convertView;
    }
}

